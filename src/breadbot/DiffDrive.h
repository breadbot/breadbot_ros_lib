#ifndef _ROS_breadbot_DiffDrive_h
#define _ROS_breadbot_DiffDrive_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace breadbot
{

  class DiffDrive : public ros::Msg
  {
    public:
      typedef float _linear_type;
      _linear_type linear;
      typedef float _angular_type;
      _angular_type angular;

    DiffDrive():
      linear(0),
      angular(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->linear);
      offset += serializeAvrFloat64(outbuffer + offset, this->angular);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->linear));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->angular));
     return offset;
    }

    const char * getType(){ return "breadbot/DiffDrive"; };
    const char * getMD5(){ return "144a16e4d6b53a0dbadc2e617460a173"; };

  };

}
#endif